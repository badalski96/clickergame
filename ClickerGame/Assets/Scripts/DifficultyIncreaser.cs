using UnityEngine;

public class DifficultyIncreaser : MonoBehaviour
{
    [SerializeField] private float _speedScale;
    [SerializeField] private float _speedLimit;
    [SerializeField] private float _timeBetweenIncrease;

    [SerializeField] private float _timer;
    private int _maxHealth;
    private float _speed;

    public int GetHpIncreaser()
    {
        return _maxHealth;
    }

    public float GetSpeedIncreaser()
    {
        return _speed;
    }

    private void FixedUpdate()
    {
        _timer += Time.fixedDeltaTime;

        if (_timer >= _timeBetweenIncrease)
        {
            _maxHealth++;

            if (_speed < _speedLimit)
            {
                _speed += _speedScale;
            }
            _timer = 0;
        }
    }
}