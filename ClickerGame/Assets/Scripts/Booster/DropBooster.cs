using UnityEngine;

public class DropBooster : MonoBehaviour
{
    [SerializeField] private int _dropChance;
    [SerializeField] private Booster[] _boosters;

    private float _forceScale = 5f;

    public void Drop(Transform unit)
    {
        System.Random rand = new System.Random();

        if (rand.Next(100) <= _dropChance)
        {
            int type = rand.Next(_boosters.Length);
            GameObject boost = Instantiate(_boosters[type].gameObject, unit.position, Quaternion.identity);
            boost.GetComponent<Rigidbody>().AddForce(Vector3.up * _forceScale, ForceMode.Impulse);
        }
    }
}