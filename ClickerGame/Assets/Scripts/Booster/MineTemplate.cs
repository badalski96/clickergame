using UnityEngine;

public class MineTemplate : MonoBehaviour
{
    private Collider[] _colliders;
    private Effect _effectPlayer;
    private bool _contact;
    private int _lethalDamage = 10000;

    private void Awake()
    {
        _effectPlayer = gameObject.GetComponent<Effect>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.TryGetComponent(out Enemy enemy))
        {
            _colliders = gameObject.GetComponents<Collider>();

            foreach (Collider collider in _colliders)
            {
                collider.enabled = true;
            }
            _contact = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(_contact)
        {
            if(other.gameObject.TryGetComponent(out Enemy enemy))
            {
                enemy.TakeDamage(_lethalDamage);
                _effectPlayer.PlayEffect(-90);
                Destroy(gameObject);
            }
        }
    }
}