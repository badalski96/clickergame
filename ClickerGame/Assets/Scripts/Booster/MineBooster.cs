using UnityEngine;

public class MineBooster : Booster
{
    [SerializeField] private MineTemplate _mineTemplate;
    [SerializeField] private Vector2 _mineRandomCount;

    private int _mineCount;
    private EnemySpawner _spawner;
    private Vector2 _borderX;
    private Vector2 _borderZ;
    private float offset = 0.5f;
    private float _surfaceY = 0.3f;
    private Vector3 _mineSpawnPoint;

    System.Random _rand = new System.Random();

    public override void ActivateBoost()
    {
        _mineCount = _rand.Next((int)_mineRandomCount.x, (int)_mineRandomCount.y);
        SpawnMineTemplate(_mineTemplate, _mineCount);
        Destroy(gameObject);
    }

    private void Awake()
    {
        _spawner = FindObjectOfType<EnemySpawner>();
        _borderX.x = _spawner.BorderX.x + offset;
        _borderX.y = _spawner.BorderX.y - offset;
        _borderZ.x = _spawner.BorderZ.x + offset;
        _borderZ.y = _spawner.BorderZ.y - offset;
    }

    private void SpawnMineTemplate(MineTemplate mineTemplate, int count)
    {
        for (int i = 0; i < count; i++)
        {
            _mineSpawnPoint.x = Random.Range(_borderX.x, _borderX.y);
            _mineSpawnPoint.y = _surfaceY;
            _mineSpawnPoint.z = Random.Range(_borderZ.x, _borderZ.y);

            Instantiate(mineTemplate, _mineSpawnPoint, Quaternion.identity);
        }
    }
}