public class KillAllEnemys : Booster
{
    private int _letalDamage = 10000;
    public override void ActivateBoost()
    {
        KillAll();
        _effectPlayer = gameObject.GetComponent<Effect>();

        if (_effectPlayer != null)
        {
            _effectPlayer.PlayEffect(90);
        }
        Destroy(gameObject);
    }

    private void KillAll()
    {
        Enemy [] enemies = FindObjectsOfType<Enemy>();
        foreach (Enemy enemy in enemies)
        {
            enemy.TakeDamage(_letalDamage);
        }
    }
}