using System.Collections;
using UnityEngine;

public class FreezBoosterTemplate : Booster
{
    protected float _freezTime;

    public override void ActivateBoost()
    {
        Debug.Log("Booster template - not booster exactly");
    }

    protected virtual IEnumerator FreezTime()
    {
        yield return null;
    }

    protected void OnActivated()
    {
        gameObject.GetComponent<Collider>().enabled = false;
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        StartCoroutine(FreezTime());
    }
}