using UnityEngine;

public abstract class Booster : MonoBehaviour
{
    protected Effect _effectPlayer;
    public abstract void ActivateBoost();
}