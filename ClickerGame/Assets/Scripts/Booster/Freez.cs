using System.Collections;
using UnityEngine;

public class Freez : FreezBoosterTemplate
{
    private EnemySpawner _spawner;

    public override void ActivateBoost()
    {
        _freezTime = 3f;
        _spawner = FindObjectOfType<EnemySpawner>();
        OnActivated();
    }

    protected override IEnumerator FreezTime()
    {
        _spawner.OnSpawnFreez(true);
        yield return new WaitForSeconds(_freezTime);
        _spawner.OnSpawnFreez(false);
        Destroy(gameObject);
        yield break;
    }
}