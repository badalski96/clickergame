using System.Collections;
using UnityEngine;

public class EnemyFreez : FreezBoosterTemplate
{
    [SerializeField] private Enemy[] _enemies;

    public override void ActivateBoost()
    {
        _freezTime = 5f;
        _enemies = FindObjectsOfType<Enemy>();
        _effectPlayer = gameObject.GetComponent<Effect>();

        if (_effectPlayer != null)
        {
            _effectPlayer.PlayEffect(90);
        }
        OnActivated();
    }

    protected override IEnumerator FreezTime()
    {
        SwitchState(false);
        yield return new WaitForSeconds(_freezTime);
        SwitchState(true);
        Destroy(gameObject);
        yield break;
    }

    private void SwitchState(bool frozen)
    {
        foreach (Enemy enemy in _enemies)
        {
            if (enemy != null)
            {
                enemy.Freeze(frozen);
            }
        }
    }
}