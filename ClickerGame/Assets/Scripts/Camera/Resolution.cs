using UnityEngine;

public class Resolution : MonoBehaviour
{
    [Range(0f, 1f)] public float WidthOrHeight = 0;

    [SerializeField] private Vector2 _defaultResolution = new Vector2(1080, 1920);
    [SerializeField] private float _targetAspect;
    [SerializeField] private float _initialFov;
    [SerializeField] private float _horizontalFov = 120f;

    private Camera _camera;
    private const float _divideBy = 1f;

    private void Start()
    {
        _camera = Camera.main;
        _targetAspect = _defaultResolution.x / _defaultResolution.y;
        _initialFov = _camera.fieldOfView;
        _horizontalFov = VerticalFov(_initialFov, _divideBy / _targetAspect);
    }

    private void Update()
    {
        float constantWidthSize = VerticalFov(_horizontalFov, _camera.aspect);
        _camera.fieldOfView = Mathf.Lerp(constantWidthSize, _initialFov, WidthOrHeight);
    }

    private float VerticalFov(float fovInDeg, float aspectRatio)
    {
        float factor = 2f;
        float hFovInRads = fovInDeg * Mathf.Deg2Rad;
        float vFovInRads = factor * Mathf.Atan(Mathf.Tan(hFovInRads / factor) / aspectRatio);

        return vFovInRads * Mathf.Rad2Deg;
    }
}