using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private GameObject _endGamePanel;

    private GameOver _gameOver;

    public void ContinueButton() => SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    public void MainMenuButton() => SceneManager.LoadScene(0);

    private void OnEnable()
    {
        _gameOver = FindObjectOfType<GameOver>();
        _gameOver.GameIsOver += OnGameOver;
    }

    private void OnDisable()
    {
        _gameOver.GameIsOver -= OnGameOver;
    }

    private void OnGameOver(bool flag)
    {
        _endGamePanel.SetActive(flag);
    }
}
