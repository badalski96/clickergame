using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject _recordTable;
    [SerializeField] private GameObject _creditsPanel;
    [SerializeField] private GameObject _menuPanel;
    public void PlayButton() => SceneManager.LoadScene(1);

    public void RecordButton()
    {
        _recordTable.SetActive(true);
        _menuPanel.SetActive(false);
    }

    public void CloseRecordTable()
    {
        _recordTable.SetActive(false);
        _menuPanel.SetActive(true);
    }

    public void CreditsButton() => _creditsPanel.SetActive(true);

    public void CloseCreditsPanel() => _creditsPanel.SetActive(false);

    public void ExitButton() => Application.Quit();

    private void Awake()
    {
        _recordTable.SetActive(false);
        _creditsPanel.SetActive(false);
    }
}
