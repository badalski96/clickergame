using TMPro;
using UnityEngine;

public class TapView : MonoBehaviour
{
    [SerializeField] private TMP_Text _text;
    private Enemy _enemy;

    private void OnEnable()
    {
        _enemy = GetComponentInParent<Enemy>();
        _enemy.CurrentHealth += OnHealthUpdate;
    }

    private void OnDisable()
    {
        _enemy.CurrentHealth -= OnHealthUpdate;
    }

    private void OnHealthUpdate(int health)
    {
        _text.text = health.ToString();
    }
}