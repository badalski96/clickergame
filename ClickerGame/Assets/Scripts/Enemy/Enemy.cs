using UnityEngine;
using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    public event UnityAction<int> CurrentHealth;

    [SerializeField] private int _maxHealth;
    [SerializeField] private float _speed;
    [SerializeField] private float _distanceToPointLimit;
    [SerializeField] private int _health;

    private Animator _animator;
    private DropBooster _dropBooster;
    private DifficultyIncreaser _difficultyIncreaser;
    private EnemySpawner _spawner;
    private GameOver _gameOver;
    private Vector3 _moveToPoint;
    private Vector2 _borderX;
    private Vector2 _borderZ;
    private float _distanceToMovePoint;
    private bool _isOver;
    private bool _isFrozen;

    public void TakeDamage(int damage)
    {
        _health -= damage;
        CurrentHealth?.Invoke(_health);

        if (_health <= 0)
        {
            _dropBooster.Drop(transform);
            _gameOver.RemoveDeadEnemy(gameObject);
            Destroy(gameObject);
        }
    }

    public void Freeze(bool frozen)
    {
        _isFrozen = !frozen;
        _animator.enabled = frozen;
    }

    private void OnEnable()
    {
        _gameOver = FindObjectOfType<GameOver>();
        _gameOver.GameIsOver += OnGameOver;
    }

    private void OnDisable()
    {
        _gameOver.GameIsOver -= OnGameOver;
    }

    private void OnGameOver(bool overFlag)
    {
        _isOver = overFlag;
        _animator.SetBool("IsOver", _isOver);
    }

    private void Start()
    {
        _spawner = FindObjectOfType<EnemySpawner>();
        _dropBooster = FindObjectOfType<DropBooster>();
        _difficultyIncreaser = FindObjectOfType<DifficultyIncreaser>();
        _animator = GetComponent<Animator>();
        _maxHealth += _difficultyIncreaser.GetHpIncreaser();
        _health = _maxHealth;
        CurrentHealth?.Invoke(_health);
        _borderX = _spawner.BorderX;
        _borderZ = _spawner.BorderZ;
        GetMovePoint();
    }

    private void Update()
    {
        if (!_isOver && !_isFrozen)
        {
            Move();
        }
    }

    private void Move()
    {
        _distanceToMovePoint = Vector3.Distance(transform.position, _moveToPoint);
        float speed = _speed + _difficultyIncreaser.GetSpeedIncreaser();

        if (_distanceToMovePoint >= _distanceToPointLimit)
        {
            transform.position = Vector3.MoveTowards(transform.position, _moveToPoint, speed * Time.deltaTime);
            transform.LookAt(_moveToPoint);
            _animator.SetFloat("Speed", speed);
        }
        else
        {
            Debug.Log($"{gameObject.name} {speed}");
            GetMovePoint();
        }
    }

    private void GetMovePoint()
    {
        _moveToPoint.x = Random.Range(_borderX.x, _borderX.y);
        _moveToPoint.z = Random.Range(_borderZ.x, _borderZ.y);
    }
}