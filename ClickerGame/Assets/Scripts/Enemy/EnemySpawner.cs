using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [field: SerializeField] public Vector2 BorderX { get; set; }
    [field: SerializeField] public Vector2 BorderZ { get; set; }

    [Header("Template")]
    [SerializeField] private Enemy[] _enemyTemplate;

    [Header("Spawn count")]
    [SerializeField] private int _min;
    [SerializeField] private int _max;

    [Header("Spawn time settings")]
    [SerializeField] private float _minTime;
    [SerializeField] private float _maxTime;
    [SerializeField] private float _diffucultyTimeDecreaser;
    [SerializeField] private float _difficultyMinTime;
    [SerializeField] private float _timer;
    [SerializeField] private bool _isFreez = false;

    private GameOver _gameOver;
    private int _enemyCount;
    private float _timeBetweenSpawn;
    private bool _isGetTime = false;
    private bool _isLose = false;

    private void OnEnable()
    {
        _gameOver.GameIsOver += OnGameOver;
    }

    private void OnDisable()
    {
        _gameOver.GameIsOver -= OnGameOver;
    }

    private void Awake()
    {
        _gameOver = GetComponent<GameOver>();
        GenerateEnemys();
    }

    private void FixedUpdate()
    {
        if (_isLose || _isFreez)
        {
            return;
        }

        if (!_isGetTime)
        {
            _timeBetweenSpawn = Random.Range(_minTime, _maxTime);
            _isGetTime = true;
        }
        _timer += Time.fixedDeltaTime;

        if (_timer >= _timeBetweenSpawn)
        {
            GenerateEnemys();
            _timer = 0;
            _isGetTime = false;

            if (_maxTime > _difficultyMinTime)
            {
                _maxTime -= _diffucultyTimeDecreaser;
            }
        }
    }

    private void GenerateEnemys()
    {
        _enemyCount = Random.Range(_min, _max);
        System.Random random = new System.Random();

        for (int i = 0; i < _enemyCount; i++)
        {
            int enemyIndex = random.Next(_enemyTemplate.Length);
            EnemySpawn(_enemyTemplate[enemyIndex], Random.Range(BorderX.x, BorderX.y), Random.Range(BorderZ.x, BorderZ.y));
        }
    }

    private void EnemySpawn(Enemy enemyTemplate, float x, float z)
    {
        float yOffset = transform.parent.localScale.y;
        GameObject enemy = Instantiate(enemyTemplate.gameObject, new Vector3(x, yOffset, z), Quaternion.identity);
        _gameOver.Enemies.Add(enemy);
    }

    private void OnGameOver(bool overFlag)
    {
        _isLose = overFlag;
    }

    public void OnSpawnFreez(bool flag)
    {
        _isFreez = flag;
    }
}