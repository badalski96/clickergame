using System.Threading.Tasks;
using UnityEngine;

public class EffectDestroyTimer : MonoBehaviour
{
    [SerializeField] private int _destroyTimeMilliseconds;

    private async void Awake()
    {
        await Task.Delay(_destroyTimeMilliseconds);
        Destroy(gameObject);
    }
}