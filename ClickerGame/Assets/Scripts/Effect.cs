using UnityEngine;

public class Effect : MonoBehaviour
{
    [SerializeField] private ParticleSystem _effect;

    public void PlayEffect(float angleX = 0, float angleY = 0, float angleZ = 0)
    {
        var effect = Instantiate(_effect, transform.position, transform.rotation);
        effect.transform.Rotate(angleX, angleY, angleZ);
        effect.Play();
    }
}