using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private int _damage;
    [SerializeField] private bool _isOver;

    private GameOver _gameOver;

    private void OnEnable()
    {
        _gameOver = FindObjectOfType<GameOver>();
        _gameOver.GameIsOver += OnGameOver;
    }

    private void OnDisable()
    {
        _gameOver.GameIsOver -= OnGameOver;
    }

    private void Update()
    {
        if (_isOver)
        {
            return;
        }    

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            { 

                if (hit.collider.gameObject.TryGetComponent(out Enemy enemy))
                {
                    enemy.TakeDamage(_damage);
                } 
                else if(hit.collider.gameObject.TryGetComponent(out Booster booster))
                {
                    booster.ActivateBoost();
                }
            }
        }
    }

    private void OnGameOver(bool overFlag)
    {
        _isOver = overFlag;
    }
}
