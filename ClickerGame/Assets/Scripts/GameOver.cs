using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameOver : MonoBehaviour
{
    public List<GameObject> Enemies;

    public event UnityAction<bool> GameIsOver;

    [SerializeField] private int _gameOverCondition;

    public void RemoveDeadEnemy(GameObject enemy)
    {
        Enemies.Remove(enemy);
    }

    private void Update()
    {
        if (Enemies.Count >= _gameOverCondition)
        {
            GameIsOver?.Invoke(true);
            Debug.Log("Game over");
        }
    }
}
